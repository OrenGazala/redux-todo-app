import { IconButton } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import React from 'react';
import { useState } from 'react';

const AddTodo = ({ addTodo }) => {
  const [content, setContent] = useState('');

  const handleChange = (e) => {
    const { value } = e.target;

    setContent(value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    addTodo(content);

    setContent('');
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <TextField
          name="content"
          type="text"
          onChange={handleChange}
          value={content}
          label="Add new todo"
        />
        <IconButton>
          <AddIcon />
        </IconButton>
      </form>
    </div>
  );
};

export default AddTodo;
