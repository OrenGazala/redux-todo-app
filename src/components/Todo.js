import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import { FormControlLabel, IconButton, makeStyles } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { deleteTodoAction } from '../actions/todo-actions/todoActions';

const useStyles = makeStyles({
  initTodo: {
    textDecoration: 'none',
  },
  checkedTodo: {
    textDecoration: 'line-through',
    color: 'lightgrey',
  },
  grow: {
    transition: 'all .3s',
    '&:hover': {
      transform: 'scale(1.2, 1.2)',
    },
  },
});

const Todo = ({ todo }) => {
  const [checked, setChecked] = useState(false);
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleDelete = (e) => {
    dispatch(deleteTodoAction(todo.id));
  };

  const handleChange = (e) => {
    setChecked(!checked);
  };

  return (
    <div className="row">
      <div className="col-6">
        <FormControlLabel
          control={
            <Checkbox id="check" className={classes.grow} checked={checked} onChange={handleChange} />
          }
          label={todo.content}
          className={checked ? classes.checkedTodo : classes.initTodo}
        />
      </div>
      <div className="col-2">
        <IconButton
          className={classes.grow}
          aria-label="delete"
          size="medium"
          color="inherit"
          onClick={handleDelete}
        >
          <DeleteIcon />
        </IconButton>
      </div>
    </div>
  );
};

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
};

export default Todo;
