import React from 'react';
import Todos from './Todos';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles({
  title: {
    textAlign: 'center',
    backgroundImage: 'linear-gradient(to right bottom, #20a02a, #91ca9b)',
    display: 'inline-block',
    color: 'transparent',
    fontSize: '2rem',
    fontWeight: 700,
    WebkitBackgroundClip: 'text',
    textTransform: 'uppercase',
    transition: 'all .3s',

    '&:hover': {
      transform: 'translateY(-3px) skewX(2deg)',
    },
  },
});

const Home = () => {
  const classes = useStyles();

  return (
    <>
      <Typography className={classes.title}>How to become a great developer</Typography>
      <Todos />
    </>
  );
};

export default Home;
