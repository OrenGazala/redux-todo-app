import React from 'react';
import Todo from './Todo';
import AddTodo from './AddTodo';
import { useDispatch, useSelector } from 'react-redux';
import { Card, CardContent, makeStyles } from '@material-ui/core';
import Count from './Count';
import { addTodoAction } from '../actions/todo-actions/todoActions';

const useStyles = makeStyles({
  card: {
    width: '80%',
    marginBottom: 20,
    boxShadow: '1px 3px 2px rgba(0,0,0, 0.2)',
  },
});

const Todos = () => {
  const todos = useSelector((state) => state.todosReducer);
  const classes = useStyles();
  const dispatch = useDispatch();

  const addTodo = (content) => {
    dispatch(addTodoAction(content));
  };

  const todoList = todos.length ? (
    todos.map((todo) => {
      return <Todo key={todo.id} todo={todo}></Todo>;
    })
  ) : (
    <p>You don't have anymore todos! :)</p>
  );

  return (
    <>
      <Card className={classes.card}>
        <CardContent>
          <Count count={todos.length} />
          {todoList}
        </CardContent>
      </Card>

      <AddTodo addTodo={addTodo} />
    </>
  );
};

export default Todos;
