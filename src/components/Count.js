import { Badge, makeStyles } from '@material-ui/core';
import React from 'react';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

const useStyles = makeStyles({
  growIcon: {
    transition: 'all .3s',

    '&:hover': {
      transform: 'scale(1.2, 1.2)',
    },
  },
});

const Count = ({ count }) => {
  const classes = useStyles();
  return (
    <div>
      <Badge className={classes.growIcon} badgeContent={count} color="primary">
        <ShoppingCartIcon />
      </Badge>
    </div>
  );
};

export default Count;
