import { ADD_TODO, DELETE_TODO } from './todoActionTypes';

export const addTodoAction = (content) => {
  return (dispatch, getState) => {
    dispatch({
      type: ADD_TODO,
      payload: {
        content,
      },
    });
  };
};

export const deleteTodoAction = (id) => {
  return (dispatch, getState) => {
    dispatch({
      type: DELETE_TODO,
      payload: {
        id,
      },
    });
  };
};
