import { DELETE_TODO, ADD_TODO } from '../actions/todo-actions/todoActionTypes';

const initState = [
  { id: 1, content: 'Clean the house' },
  { id: 2, content: 'Learn ReactJs' },
];

const todosReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return addTodo(state, action.payload);

    case DELETE_TODO:
      return deleteTodo(state, action.payload);

    default:
      return state;
  }
};

const addTodo = (state, payload) => {
  const { content } = payload;

  let todo = {
    content,
    id: Math.random() * 10,
  };

  return [...state, todo];
};

const deleteTodo = (state, payload) => {
  const { id } = payload;
  return state.filter((todo) => todo.id !== id);
};

export default todosReducer;
