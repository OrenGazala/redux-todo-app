import React from 'react';
import './App.css';
import Home from './components/Home';
import { hot } from 'react-hot-loader/root';

const App = () => {
  return (
    <div className="App">
      <Home />
    </div>
  );
};

export default process.env.NODE_ENV === 'development' ? hot(App) : App;
